﻿using System.Web.UI.WebControls;
using System.Data;


namespace J3DHelpers.Controls
{
   public class  DDL
    {
        ///<Summary>
        /// Remove item from DDL by value
        ///</Summary>
        public static DropDownList CleanDDLValue(DropDownList ddl, string s)
        {
            ddl.Items.Remove(ddl.Items.FindByValue(s));
            return ddl;

        }

        ///<Summary>
        /// Remove item from DDL by value
        ///</Summary>
        public static DropDownList CleanDDLText(DropDownList ddl, string s)
        {
            ddl.Items.Remove(ddl.Items.FindByText(s));
            return ddl;

        }

        /// <summary>
        ///  ddlMyDDL = J3DHelpers.Controls.DDL.PopulateDDLFromDatatable(ddlMyDDL, _dt, "job_id", "job_title", "Select Job");
        /// </summary>
        /// <param name="_ddl">Dropdownlist control to populate</param>
        /// <param name="_dt">Datatable to use for population</param>
        /// <param name="_valcol">Name of column in _dt to use for listitem.value</param>
        /// <param name="_textcol">Name of column in _dt to use for listitem.text</param>
        /// <param name="_HeaderSelect"> Optional, enter "Select Name" for the first row (-1) to be a dummy selector value of "Select Name"</param>

        /// <returns>DropDownList</returns>
        public static DropDownList PopulateDDLFromDatatable(DropDownList _ddl, DataTable _dt, string _valcol, string _textcol, string _HeaderSelect)
        {
            _ddl.Items.Clear();
            _ddl.DataSource = _dt;
            _ddl.DataValueField = _valcol;
            _ddl.DataTextField = _textcol;
            _ddl.DataBind();
            _dt.Dispose();

            if (_HeaderSelect != "")  //add a default value
            {
                _ddl.Items.Insert(0, new ListItem(_HeaderSelect, "0"));
            }

            return _ddl;
        }

      
 
    }
}
