﻿using System.Data;

namespace J3DHelpers.Data
{
   public class J3DataTables
    {

        public static void CleanDataTable(ref DataTable _DTToClean, DataTable _DTSource, string _ToCleanCol, string _SourceCol)
        {
            foreach (DataRow srow in _DTSource.Rows)
            {
                foreach (DataRow crow in _DTToClean.Rows)
                {
                    if (srow[_SourceCol].ToString() == crow[_ToCleanCol].ToString())
                    {
                        _DTToClean.Rows.Remove(crow);
                        //                        crow.Delete();
                        break;
                    }
                }

            }
        }


   




    }
}
