﻿using System.Linq;


namespace J3DHelpers.Strings
{
   public class Clean
    {

        /// <summary>
        /// Pulls out unicode chars
        /// </summary>
        /// <param name="inputValue"></param>
        /// <returns></returns>
        public static string StripUnicodeCharactersFromString(string inputValue)
        {
            //http://codesnippets.fesslersoft.de/how-to-remove-unicode-characters-from-a-string-in-c-and-vb-net/
            return new string(inputValue.Where(c => c <= sbyte.MaxValue).ToArray()).Trim();

        }

        /// <summary>
        /// Replaces apostrophes for DB insertion
        /// </summary>
        /// <param name="_s"></param>
        /// <returns></returns>
        public static string CleanDBStringIn(string _s)
        {
            return _s.Replace("'", "''");
        }
    }
}
