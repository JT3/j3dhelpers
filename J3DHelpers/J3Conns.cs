﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
//using J3DHelpers.DBConns;
using MySql.Data.MySqlClient;

namespace J3DHelpers
{
    /// <summary>
    /// Handles database connections and returns
    /// </summary>
     public partial class J3Conns
    {

    

            /// <summary>
            /// Class to pass parameters to DB.  SQL should then be passed as "exec sp_something x=@P1"
            /// </summary>
           public  class dbparam
        {
            /// <summary>
            /// Value of parameter being passed. 
            /// </summary>
            public object value { get; set; }

            /// <summary>
            /// 0=string, 1=int, 2=decimal 
            /// </summary>
            public int datatype { get; set; }
        }

        /// <summary>
        /// Lazy way to add items to a list
        /// </summary>
        /// <param name="_value">Value of parameter being passed</param>
        /// <param name="_datatype"> 0=string, 1=int, 2=decimal,  3=bit </param>
        /// <param name="_dbp">Existing dbparam list</param>
        /// <returns>Lis</returns>
        public static List<dbparam> MakeDBParams(object _value, int _datatype, List<dbparam> _dbp)
        {
            _dbp.Add(new dbparam
            {
                value = _value,
                datatype = _datatype
            });
            return _dbp;
        }


        /// <summary>
        ///  Returns a datatable without the overhead
        /// </summary>
        /// <param name="_query"> exec sp_Something @SurveyID=@P0, @PersonID=@P1</param>
        /// <param name="_params">Ordered list of parameters and values.  item 0 becomes @P0,1=@P1 etc</param>
        /// <param name="_dbtype">0=sqlserver, 1=mysql</param>
        /// <param name="_conn"> Connection string</param>
        /// <param name="_exception"> to check for errors</param>
        /// <returns>DataTable </returns>
        public static DataTable GetDataTable(string _query, List<dbparam> _params, int _dbtype, string _conn)
        {
            DataTable _dt = new DataTable();
           
            try
            {

                if (_dbtype == 0) //sqlserver
                {
                    // SqlDataReader _dr = DBConns.SQLDB.GetSQLDataReader("xxx", _params, _conn);
                    //_dt.Load(DBConns.SQLDB.GetSQLDataReader(_query, _params, _conn, out _sql));
                    using (SqlConnection conn = new SqlConnection(_conn))
                    {
                        SqlCommand cmd = ProcessSqlCommand(_query, _params);
                       // _sql = cmd.CommandText;

                        cmd.Connection = conn;
                        cmd.CommandType = CommandType.Text;

                        //JJT 8/7/18  added arithabort on  from https://dba.stackexchange.com/questions/2500/make-sqlclient-default-to-arithabort-on
                        cmd.CommandText = "SET ARITHABORT ON;\n" + cmd.CommandText;
                        
                        
                        //cmd.Connection = sqlConnection1;
                        conn.Open();
                        //DataTable tblCols = new DataTable();
                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            _dt.Load(dr);
                        }
                    }
                }
                else //mysql
                //TODO:  Do this like the SQL above.  Datareader closes before table is created so no deal.
                {
                    using (MySqlConnection conn = new MySqlConnection(_conn))
                    {
                        MySqlCommand cmd = ProcessMySqlCommand(_query, _params);
                        // _sql = cmd.CommandText;

                        cmd.Connection = conn;
                        cmd.CommandType = CommandType.Text;
                        //cmd.Connection = sqlConnection1;
                        conn.Open();
                        //DataTable tblCols = new DataTable();
                        using (MySqlDataReader dr = cmd.ExecuteReader())
                        {
                            _dt.Load(dr);
                        }
                    }

                }
            }
            catch(Exception e)
            {
                throw; // new Exception(e.ToString() + "   " + e.InnerException.ToString());
            }
            return _dt;
        }


        /// <summary>
        ///  Returns a dataset without the overhead
        /// </summary>
        /// <param name="_query"> exec sp_Something @SurveyID=@P0, @PersonID=@P1</param>
        /// <param name="_params">Ordered list of parameters and values.  item 0 becomes @P0,1=@P1 etc</param>
        /// <param name="_dbtype">0=sqlserver, 1=mysql</param>
        /// <param name="_conn"> Connection string</param>
        /// <returns>DataSet </returns>
        public static DataSet GetDataSet(string _query, List<dbparam> _params, int _dbtype, string _conn)
        {
            //_sql = string.Empty;

            DataSet _ds = new DataSet();
           
            try
            {

                if (_dbtype == 0) //sqlserver
                {
                    // SqlDataReader _dr = DBConns.SQLDB.GetSQLDataReader("xxx", _params, _conn);
                    //_dt.Load(DBConns.SQLDB.GetSQLDataReader(_query, _params, _conn, out _sql));
                    using (SqlConnection conn = new SqlConnection(_conn))
                    {
                        SqlCommand cmd = ProcessSqlCommand(_query, _params);
                        SqlDataAdapter da = new SqlDataAdapter();
                        // _sql = cmd.CommandText;

                        cmd.Connection = conn;
                        cmd.CommandType = CommandType.Text;
                        da.SelectCommand = cmd;

                        //JJT 8/7/18  added arithabort on  from https://dba.stackexchange.com/questions/2500/make-sqlclient-default-to-arithabort-on
                        cmd.CommandText = "SET ARITHABORT ON;\n" + cmd.CommandText;


                        //cmd.Connection = sqlConnection1;
                        conn.Open();
                        da.Fill(_ds);
                        //DataTable tblCols = new DataTable();
                        //using (SqlDataReader dr =  cmd.ExecuteReader())
                        //{
                        //    _ds.Load(dr,);
                        //}
                    }
                }
                else //mysql
                //TODO:  Do this like the SQL above.  Datareader closes before table is created so no deal.
                {
                    using (MySqlConnection conn = new MySqlConnection(_conn))
                    {
                        MySqlCommand cmd = ProcessMySqlCommand(_query, _params); //GetMySqlcommand(_conn))
                        MySqlDataAdapter da = new MySqlDataAdapter();
                        // _sql = cmd.CommandText;

                        cmd.Connection = conn;
                        cmd.CommandType = CommandType.Text;
                        da.SelectCommand = cmd;
                        //cmd.Connection = sqlConnection1;
                        conn.Open();
                        da.Fill(_ds);
                    }
                }
            }
            catch (Exception e)
            {
                throw; //new Exception(e.ToString() + "   " + e.InnerException.ToString());
            }
            return _ds;
        }


        /// <summary>
        /// Returns a single string
        /// </summary>
        /// <param name="_query">SQL</param>
        /// <param name="_params">Ordered list of parameters and values.  item 0 becomes @P0,1=@P1 etc</param>
        /// <param name="_dbtype">0=sqlserver, 1=mysql</param>
        /// <param name="_conn"> Connection string</param>
        /// <returns>String</returns>
        public static object  GetScalar(string _query, List<dbparam> _params, int _dbtype, string _conn)
        {
           
            string ret = string.Empty;
            try
            {

                if (_dbtype == 0) //sqlserver
                {
                    // SqlDataReader _dr = DBConns.SQLDB.GetSQLDataReader("xxx", _params, _conn);
                    //_dt.Load(DBConns.SQLDB.GetSQLDataReader(_query, _params, _conn, out _sql));
                    using (SqlConnection conn = new SqlConnection(_conn))
                    {
                        SqlCommand cmd = ProcessSqlCommand(_query, _params);
                        // _sql = cmd.CommandText;

                        cmd.Connection = conn;
                        cmd.CommandType = CommandType.Text;

                        //JJT 8/7/18  added arithabort on  from https://dba.stackexchange.com/questions/2500/make-sqlclient-default-to-arithabort-on
                        cmd.CommandText = "SET ARITHABORT ON;\n" + cmd.CommandText;

                        //cmd.Connection = sqlConnection1;
                        conn.Open();
                        //DataTable tblCols = new DataTable();
                        
                        var x = cmd.ExecuteScalar();
                        return x;

                    }
                }
                else //mysql
                {
                    using (MySqlConnection conn = new MySqlConnection(_conn))
                    {
                        MySqlCommand cmd = ProcessMySqlCommand(_query, _params); //GetMySqlcommand(_conn))
                        cmd.Connection = conn;
                        cmd.CommandType = CommandType.Text;
                        conn.Open();
                        var r = cmd.ExecuteScalar();
                        ret = r.ToString();

                    }
                }
                
            }
            catch (Exception e)
            {
                throw new Exception(e.ToString());
            }
            return ret;
        }


        ///// <summary>
        /////  Returns a DataReader without the overhead
        ///// </summary>
        ///// <param name="_query"> exec sp_Something @SurveyID=@P0, @PersonID=@P1</param>
        ///// <param name="_params">Ordered list of parameters and values.  item 0 becomes @P0,1=@P1 etc</param>
        ///// <param name="_dbtype">0=sqlserver, 1=mysql</param>
        ///// <param name="_conn"> Connection string</param>
        ///// <param name="_exception"> to check for errors</param>
        ///// <returns>IDataReader</returns>
        //public static object GetDataReader(string _query, List<dbparam> _params, int _dbtype, string _conn,  out string _exception, out string _sql)
        //{
        //    _sql = string.Empty;
        //    _exception = "0";
        //    DataTable _dt = new DataTable();
        //    IDataReader d = null;

        //    try
        //    { 
        //    if (_dbtype == 0)
        //    {

        //      //  d = DBConns.SQLDB.GetSQLDataReader(_query, _params, _conn, out _sql);
        //    }
        //    else
        //    {
        //            //do the mysql version here
        //            MySQLDB m = new MySQLDB();
        //            // d =  DBConns.MySQLDB.GetMYSQLDataReader(_query, _params, _conn);
        //            d = m.GetMYSQLDataReader(_query, _params, _conn);
        //        }
        //    }
        //    catch(Exception e)
        //    {
        //        _exception = e.ToString();
        //    }
        //   return d;
        //}







        //public class Part : IEquatable<Part>
        //{
        //    public string PartName { get; set; }

        //    public int PartId { get; set; }

        //    public override string ToString()
        //    {
        //        return "ID: " + PartId + "   Name: " + PartName;
        //    }
        //    public override bool Equals(object obj)
        //    {
        //        if (obj == null) return false;
        //        Part objAsPart = obj as Part;
        //        if (objAsPart == null) return false;
        //        else return Equals(objAsPart);
        //    }
        //    public override int GetHashCode()
        //    {
        //        return PartId;
        //    }
        //    public bool Equals(Part other)
        //    {
        //        if (other == null) return false;
        //        return (this.PartId.Equals(other.PartId));
        //    }
        //    // Should also override == and != operators.

        //}


        /// <summary>
        ///  returns id number for inserts, or 0 for other queries.
        /// </summary>
        /// <param name="_query"> exec sp_Something @SurveyID=@P0, @PersonID=@P1</param>
        /// <param name="_params">Ordered list of parameters and values.  item 0 becomes @P0,1=@P1 etc</param>
        /// <param name="_dbtype">0=sqlserver, 1=mysql</param>
        /// <param name="_conn"> Connection string</param>
        /// <param name="_exception"> to check for errors</param>
        /// <returns>DataTable </returns>
        public static long ExecNonQuery(string _query, List<dbparam> _params, int _dbtype, string _conn)
        {
            //_sql = string.Empty;
            long ret = 0;
            
            try
            {

                if (_dbtype == 0) //sqlserver
                {
                    using (SqlConnection conn = new SqlConnection(_conn))
                    {
                        SqlCommand cmd = ProcessSqlCommand(_query, _params);
                        cmd.Connection = conn;
                        cmd.CommandType = CommandType.Text;

                        //JJT 8/7/18  added arithabort on  from https://dba.stackexchange.com/questions/2500/make-sqlclient-default-to-arithabort-on
                        cmd.CommandText = "SET ARITHABORT ON;\n" + cmd.CommandText;

                        conn.Open();
                        ret = cmd.ExecuteNonQuery();
                        
                    }
                }
                else //mysql
                {
                    using (MySqlConnection conn = new MySqlConnection(_conn))
                    {
                        MySqlCommand cmd = ProcessMySqlCommand(_query, _params); //GetMySqlcommand(_conn))

                       // string commandline = _query;

                        cmd.Connection = conn;
                        cmd.CommandType = CommandType.Text;
                        conn.Open();
                        cmd.ExecuteNonQuery();
                        ret = cmd.LastInsertedId;
                    }

                }
            }
            catch (Exception e)
            {
                throw;
            }
           
                return ret;
           
            
        }

    }
}
