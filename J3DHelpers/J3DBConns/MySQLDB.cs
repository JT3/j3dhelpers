﻿using System;
using System.Collections.Generic;
using System.Linq;
using MySql.Data.MySqlClient;

namespace J3DHelpers
{
   partial class J3Conns
    {


        private static MySqlCommand GetMySqlcommand(string _conn)
        {
            MySqlConnection connect = new MySqlConnection(_conn);
            MySqlCommand cmd = new MySqlCommand();
            try
            {
                cmd.Connection = connect;
                cmd.Connection.Open();
                return cmd;
            }
            catch (Exception e)
            {
                //                logger.Error(e.ToString());
                return cmd;
            }
        }




        //public  MySqlDataReader GetMYSQLDataReader(string _query, List<J3Conns.dbparam> _params, string _conn)
        //{
        //    MySqlDataReader _dr = null;
        //    MySqlCommand sc = ProcessSqlCommand(_query, _params);
        //    using (MySqlConnection conn = new MySqlConnection(_conn))
        //    {
        //        MySqlCommand cmd = ProcessSqlCommand(_query, _params);
        //        try
        //        {
        //            conn.Open();
        //            _dr = cmd.ExecuteReader();
        //        }
        //        catch (System.Exception ex)
        //        {

        //        }

        //    }
        //    return _dr;

        //}



        private static MySqlCommand ProcessMySqlCommand(string _query, List<J3Conns.dbparam> _params)
        {

            MySqlCommand cmd = new MySqlCommand(); // = new MySqlCommand())
            {
                //try
                //{
                string commandline = _query;
                int count = 0;
                cmd.CommandText = commandline;
                foreach (J3Conns.dbparam d in _params)
                {
                    string _p = "@P" + count.ToString();
                    //                    cmd.Parameters.AddWithValue(_p, d.value);
                    cmd.Parameters.Add(_p, SetMysqlDataType(d.datatype));
                    cmd.Parameters[_p].Value = SetMysqlDataValue(d.datatype, d.value);

                    count += 1;
                }
                return cmd;
                //}
                //catch (Exception ex)
                //{

                //}
            }

        }



        private static object SetMysqlDataValue(int _datatype, object _value)
        {
            object _t;
            if (_value == null)
            {
                _t = DBNull.Value; // System.DBNull;
            }
            else
            {
                switch (_datatype)
                {
                    case 0:
                        _t = _value.ToString();
                        break;
                    case 1:
                        _t = int.Parse(_value.ToString()); // SqlDbType.Int;
                        break;
                    case 2:
                        _t = double.Parse(_value.ToString()); //SqlDbType.Decimal;
                        break;
                    case 3:
                        _t = _value;
                        break;

                    default:
                        _t = _value.ToString();
                        break;
                }
            }
            return _t;

        }

        private static MySqlDbType SetMysqlDataType(int _datatype)
        {
            MySqlDbType _t;
            switch (_datatype)
            {
                case 0:
                    _t = MySqlDbType.VarChar;
                    break;
                case 1:
                    _t = MySqlDbType.Int32;
                    break;
                case 2:
                    _t = MySqlDbType.Decimal;
                    break;
                case 3:
                    _t = MySqlDbType.LongText;
                    break;
                default:
                    _t = MySqlDbType.VarChar;
                    break;
            }

            return _t;

        }







    }
}
