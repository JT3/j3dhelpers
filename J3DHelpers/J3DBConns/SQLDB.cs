﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;

namespace J3DHelpers
{
    partial class J3Conns
    {

        // public static SqlDataReader  GetSQLDataReader(string _query, List<Connections.dbparam> _params, string _conn, out string _sql)
        //{
        //    SqlDataReader _dr = null;
        // //  SqlCommand sc = ProcessSqlCommand(_query, _params);
        //    using (SqlConnection conn = new SqlConnection(_conn))
        //    {
        //        SqlCommand cmd = ProcessSqlCommand(_query, _params);
        //        _sql = cmd.CommandText;
               
        //        try
        //        {
        //            conn.Open();
        //            _dr =  cmd.ExecuteReader();
        //        }
        //        catch (System.Exception ex)
        //        {
                    
        //        }
              
        //    }
        //    return _dr;

        //}

        /// <summary>
        /// creates parameters and values to the @P0, @P1... values with the dbparam list
        /// </summary>
        /// <param name="_query">SQL query to be processed ("exec sp_something @surveyid=@P0, @personid=@P1</param>
        /// <param name="_params">list of dbparams to add to the sqlcommand</param>
        /// <returns></returns>
        public static SqlCommand ProcessSqlCommand(string _query, List<J3Conns.dbparam> _params)
        {
            SqlCommand cmd = new SqlCommand(_query);
            int count = 0;
            foreach(J3Conns.dbparam d in _params)
            {
                string _p = "@P" +  count.ToString();
                cmd.Parameters.Add(_p, SetDataType(d.datatype));
                cmd.Parameters[_p].Value = SetDataValue(d.datatype, d.value);

                count += 1;
            }
            return cmd;
        }

      
        private static object SetDataValue(int _datatype, object _value)
        {
            object _t;
            if (_value == null)
            {
                _t = DBNull.Value; // System.DBNull;
            }
            else
            {
                switch (_datatype)
                {
                    case 0:
                        _t = _value.ToString();
                        break;
                    case 1:
                        _t = int.Parse(_value.ToString()); // SqlDbType.Int;
                        break;
                    case 2:
                        _t = double.Parse(_value.ToString()); //SqlDbType.Decimal;
                        break;
                    case 3:  //bool
                        _t = _value;
                        break;
                    default:
                        _t = _value.ToString();
                        break;
                }
            }
            return _t;

        
        }

        private static SqlDbType SetDataType( int _datatype)
        {
            SqlDbType _t;
            switch(_datatype)
            { 
                case 0:
                    _t = SqlDbType.NVarChar;
                    break;
                case 1:
                    _t = SqlDbType.Int;
                    break;
                case 2:
                    _t = SqlDbType.Decimal;
                    break;
                case 3:
                    _t = SqlDbType.Bit;
                    break;
                default:
                    _t = SqlDbType.NVarChar;
                    break;
            }

            return _t;

        }


    }
}
