﻿using System.Collections.Generic;
using System.Net.Mail;

namespace J3DHelpers.Mail
{
    public class SMTP
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="_body"></param>
        /// <param name="_to">List<string></string></param>
        /// <param name="_cc">List<string></param>
        /// <param name="_bcc">List<string></param>
        /// <param name="_from"></param>
        /// <param name="_subject"></param>
        /// <param name="_isHTML"></param>
        /// <param name="_SMTPLogin"></param>
        /// <param name="_SMTPPass"></param>
        /// <param name="_SMTPSender"></param>
        /// <param name="_SMTPServer"></param>
        /// <param name="_AttachmentPath">List<string> with local path to attachment</param>
        public static void SendSMTP(string _body, List<string> _to, List<string> _cc, List<string> _bcc, string _from, string _subject, bool _isHTML, string _SMTPLogin, string _SMTPPass, string _SMTPSender, string _SMTPServer, List<string> _AttachmentPath)
        {

            MailMessage mm = new MailMessage();

           
            foreach (var address in _to)
            {
                mm.To.Add(address);
            }
            foreach (var address in _cc)
            {
                mm.CC.Add(address);
            }
            foreach (var address in _bcc)
            {
                mm.Bcc.Add(address);
            }
            SmtpClient smtpserver = new SmtpClient(_SMTPServer);

            foreach (var path in _AttachmentPath)
            {
                Attachment attachFile = new Attachment(path);
                mm.Attachments.Add(attachFile);
            }


            mm.Subject = _subject;
            mm.Body = _body;
            mm.IsBodyHtml = _isHTML;
            mm.From = new MailAddress(_SMTPLogin, _SMTPSender);

            smtpserver.Port = 587;
            smtpserver.Credentials = new System.Net.NetworkCredential(_SMTPLogin, _SMTPPass);
            smtpserver.EnableSsl = true;


            smtpserver.Send(mm);

        }




    }
}
